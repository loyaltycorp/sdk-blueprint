<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Traits;

trait Primable
{
    /**
     * All the attributes for this repository
     *
     * @var array
     */
    protected $attributes = ['*'];

    /**
     * Items in this series
     *
     * @var array
     */
    private $items = [];

    /**
     * The primary key for this repository
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Whether this repository only contains the primary value or not
     *
     * @var bool
     */
    protected $primaryOnly = false;

    /**
     * Assert the primary key exists in the attribute array
     *
     * @return void
     */
    private function assertPrimaryKey() : void
    {
        $this->addAttributes([$this->getPrimaryKey()]);
    }

    /**
     * Convert array to a primary array
     *
     * @param array $data The data to process
     *
     * @return array The processed array
     */
    private function convertToPrimary(array $data) : array
    {
        // Set primary flag
        $this->primaryOnly = true;

        // Convert data to contain primary key
        return [$this->getPrimaryKey() => reset($data)];
    }

    /**
     * Get the primary key attribute
     *
     * @return string
     */
    public function getPrimaryKey() : string
    {
        // Check attributes for case insensitivity, will allow a primary key of
        // id to be matched to an attribute of ID
        foreach ($this->attributes as $attribute) {
            // If attribute matches primary key, use what has been found
            if (mb_strtolower($attribute) === mb_strtolower($this->primaryKey)) {
                return $attribute;
            }
        }

        // Use primary key as is
        return $this->primaryKey;
    }

    /**
     * Get the value from the primary key
     *
     * @return mixed
     */
    public function getPrimaryValue()
    {
        return $this->items[$this->getPrimaryKey()] ?? null;
    }

    /**
     * Determine if an attribute is the primary key or not
     *
     * @param string $attribute The attribute to check
     *
     * @return bool
     */
    public function isPrimaryKey(string $attribute) : bool
    {
        return mb_strtolower($attribute) === mb_strtolower($this->getPrimaryKey());
    }

    /**
     * Whether this is a primary only repository or not
     *
     * @return bool
     */
    public function primaryOnly() : bool
    {
        return $this->primaryOnly;
    }
}
