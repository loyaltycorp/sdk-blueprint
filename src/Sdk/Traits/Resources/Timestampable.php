<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources;

trait Timestampable
{
    /**
     * Set created at date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return mixed This instance
     */
    public function whereCreatedAt(string $date) : self
    {
        return $this->where('created_at', $this->formatDate($date, 'Y-m-d H:i:s'));
    }

    /**
     * Set updated at date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return mixed This instance
     */
    public function whereUpdatedAt(string $date) : self
    {
        return $this->where('updated_at', $this->formatDate($date, 'Y-m-d H:i:s'));
    }
}
