<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use LoyaltyCorp\SdkBlueprint\Sdk\Validation\Validator;

trait Validatable
{
    /**
     * Errors returned by the validator
     *
     * @var array
     */
    private $errors = [];

    /**
     * Items in this series
     *
     * @var array
     */
    private $items = [];

    /**
     * Repository validation rules
     *
     * @var array
     */
    public $rules = [];

    /**
     * Validation instance
     *
     * @var \LoyaltyCorp\SdkBlueprint\Sdk\Validation\Validator
     */
    private $validator;

    /**
     * Get method based rules
     *
     * @param string $method The method being called
     *
     * @return array
     */
    public function getRules(string $method = null) : array
    {
        $rules = [[]];

        // Process each rule
        foreach ($this->rules as $key => $rule) {
            // If rule isn't an array add and continue
            if (!is_array($rule)) {
                $rules[] = [$key => $rule];
                continue;
            }

            // Rule is an array, check if method matches
            $methods = explode(',', mb_strtolower($key));

            // If method is in methods array, use rule
            if (in_array(mb_strtolower($method), $methods, true)) {
                $rules[] = $this->rules[$key];
            }
        }

        return array_merge(...$rules);
    }

    /**
     * Get errors from the last validation
     *
     * @return array Validation errors
     */
    public function getValidationErrors() : array
    {
        return $this->errors;
    }

    /**
     * Get validation instance, creates the instance if it hasn't previously been created
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Validation\Validator The validation instance
     */
    private function getValidatorInstance() : Validator
    {
        // If validator isn't instantiated, create it
        if (!$this->validator instanceof Validator) {
            $this->validator = new Validator;
        }

        return $this->validator;
    }

    /**
     * Perform validation on this repository
     *
     * @param string $method The method to validate against
     *
     * @return bool The validation status
     */
    public function validate(string $method = null) : bool
    {
        // Perform validation on repository
        $this->getValidatorInstance()->validate($this->items, $this->getRules($method));

        // Validate subitems and merge errors together
        $this->errors = array_merge(
            $this->getValidatorInstance()->getErrors(),
            $this->validateArray($this->items, $method)
        );

        // Result is based on error count
        return !count($this->errors);
    }

    /**
     * Validate an array recursively
     *
     * @param array $array The array of items to validate
     * @param string $method The method to validate against
     *
     * @return array Any errors found
     */
    private function validateArray(array $array, string $method = null) : array
    {
        $errors = [];

        // Process array recursively
        foreach ($array as $key => $value) {
            // Recurse into collections
            if ($value instanceof Collection) {
                $errors[$key] = $this->validateArray($value->getItems(), $method);
                continue;
            }

            // If value is a repository, validate
            if ($value instanceof Repository) {
                $value->validate($method);
                $errors[$key] = $value->getValidationErrors();
            }
        }

        // Remove empty errors
        return array_filter($errors);
    }
}
