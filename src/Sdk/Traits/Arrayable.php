<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use ArrayIterator;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\SeriesInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

trait Arrayable
{
    /**
     * Items in this series
     *
     * @var array
     */
    private $items = [];

    /**
     * Convert series to string
     *
     * @return string
     */
    public function __toString() : string
    {
        return json_encode($this->toArray());
    }

    /**
     * Get the number of items in this series
     *
     * @return int The number of items in this series
     */
    public function count() : int
    {
        return count($this->items);
    }

    /**
     * Get an iterator for the items
     *
     * @return \ArrayIterator
     */
    public function getIterator() : ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    /**
     * Recursively convert a series to array
     *
     * @return array All data from the series in array format
     */
    public function toArray() : array
    {
        // Don't change original items array
        $array = $this->items;

        foreach ($array as $key => $value) {
            // If value is a repository with a single value which is primary, use that
            // otherwise convert series to array
            if ($value instanceof SeriesInterface) {
                $array[$key] = $value instanceof Repository && $value->primaryOnly() ?
                    $value->getPrimaryValue() :
                    $value->toArray();
            }

            // If value is null, remove
            if ($value === null) {
                unset($array[$key]);
            }
        }

        return $array;
    }
}
