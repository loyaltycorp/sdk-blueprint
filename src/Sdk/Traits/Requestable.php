<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\AttributeNotSetException;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\MethodNotSupportedException;
use Illuminate\Support\Str;

trait Requestable
{
    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [];

    /**
     * Define the versions for each endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpointVersions = [
        'delete' => 1,
        'get' => 1,
        'post' => 1,
        'put' => 1,
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    protected $mappings = [];

    /**
     * Define the parent id for this entity, used where an entity is a child of
     * another entity
     */
    protected $parentId;

    /**
     * Define the intial resource for this entity, used where the same data is used
     * for multiple endpoints, e.g. the customer/merchant crossover
     *
     * @var string
     */
    protected $resource;

    /**
     * Define the resource id for this entity, used where an entity is a child of
     * a resource, usually a customer or merchant
     */
    protected $resourceId;

    /**
     * Get mappings for responses
     *
     * @param string $method The method being used
     *
     * @return array The array from $this->mappings
     */
    public function getMappings(string $method = null) : array
    {
        // Lowercase the method
        if ($method !== null) {
            $method = mb_strtolower($method);
        }

        return $this->mappings[$method] ?? $this->mappings;
    }

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\MethodNotSupportedException If the method requested is not valid
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // Lowercase the method
        $method = mb_strtolower($method);

        // If endpoint doesn't exist, throw exception
        if (!array_key_exists($method, $this->endpoints)) {
            throw new MethodNotSupportedException("The method '$method' is not supported on this resource");
        }

        // Grab endpoint and ensure leading and trailing slashes
        $endpoint = sprintf('/%s/', trim($this->endpoints[$method], '/'));

        // Extract variables from the endpoint
        preg_match_all('#(/:([\da-zA-Z_]+)(?![^/]))#', $endpoint, $variables);

        // If there are no matches return original endpoint
        if (!isset($variables[1]) || !count($variables[1])) {
            return trim($this->endpoints[$method], '/');
        }

        return $this->getFinalEndpoint($endpoint, $variables);
    }

    /**
     * Get the endpoint version for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint version for the method
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\MethodNotSupportedException If the method requested is not valid
     */
    public function getEndpointVersionFromMethod(string $method) : string
    {
        // Lowercase the method
        $method = mb_strtolower($method);

        // If endpoint doesn't exist, throw exception
        if (!array_key_exists($method, $this->endpointVersions)) {
            throw new MethodNotSupportedException("The method '$method' is not supported on this resource");
        }

        return (string)$this->endpointVersions[$method];
    }

    /**
     * Get the endpoint with variables replaced
     *
     * @param string $endpoint The endpoint to parse
     * @param array $variables Variables which require replacement within the endpoint
     *
     * @return string The parsed/final endpoint
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\AttributeNotSetException If the endpoint requires an attribute which is empty
     */
    private function getFinalEndpoint(string $endpoint, array $variables) : string
    {
        // Process each variable
        foreach ($variables[2] as $index => $variable) {
            // If variable is special, attempt to handle directly
            $attribute = Str::camel($variable);
            if (isset($this->$attribute)) {
                $endpoint = str_replace($variables[1][$index] . '/', sprintf('/%s/', $this->$attribute), $endpoint);
                continue;
            }

            // Ensure the variable has a value
            if ($this->get($variable) === null) {
                $message = "The attribute '$variable' does not exist on this resource but is required by API";
                throw new AttributeNotSetException($message);
            }

            $endpoint = str_replace($variables[1][$index] . '/', sprintf('/%s/', $this->get($variable)), $endpoint);
        }

        // Return endpoint without leading and trailing slashes
        return trim($endpoint, '/');
    }

    /**
     * Get resource endpoint from an entity id
     *
     * @param string $entityId The id of the entity to check
     *
     * @return void
     */
    protected function getResourceEndpointFromEntityId(string $entityId) : void
    {
        // Customers
        if (preg_match('/^cus_[\da-zA-Z]+$/', $entityId, $matches)) {
            $this->resource = 'customers';
        }

        // Merchants
        if (preg_match('/^mer_[\da-zA-Z]+$/', $entityId, $matches)) {
            $this->resource = 'merchants';
        }
    }

    /**
     * Remap response fields from one key to another
     *
     * @param array $data The data array to remap
     * @param string $method The method being used
     *
     * @return array
     */
    public function remapResponse(array $data, string $method = null) : array
    {
        if (count($this->getMappings($method))) {
            foreach ($this->getMappings($method) as $from => $to) {
                if (array_key_exists($from, $data)) {
                    $data[$to] = $data[$from];
                    unset($data[$from]);
                }
            }
        }

        return $data;
    }

    /**
     * Set resource, resource id and parent id from a data array
     *
     * @param array|null $data The data to exteact the information from
     * @param array $resourceIdFields The fields which can contain the resource id
     * @param array $parentIdFields The fields which can contain the parent id
     *
     * @return void
     */
    protected function setEndpointVariablesFromData(
        ?array $data,
        array $resourceIdFields,
        array $parentIdFields = []
    ) : void {
        // If data isn't an array, do nothing
        if (!is_array($data)) {
            return;
        }

        // Attempt to find resource and resource id
        foreach ($resourceIdFields as $resourceIdField) {
            // If id field exists, use as resource id and set resource type
            if (array_key_exists($resourceIdField, $data)) {
                $this->resourceId = $data[$resourceIdField];
                $this->getResourceEndpointFromEntityId($this->resourceId);
                break;
            }
        }

        // Attempt to find parent id
        foreach ($parentIdFields as $parentIdField) {
            // If id field exists, use as parent id
            if (array_key_exists($parentIdField, $data)) {
                $this->parentId = $data[$parentIdField];
                break;
            }
        }
    }
}
