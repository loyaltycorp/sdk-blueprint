<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Illuminate\Support\Str;

trait Attributable
{
    /**
     * All the attributes for this repository
     *
     * NOTE: Due to the way getters and setters worked you shouldn't have a camelCase and snake_case key which
     * resolve to the same thing, e.g. transactionID and transaction_id are the same key, the getters and setters
     * will only ever use the camel case version as the snake case version is only accessed if the camel case version
     * does not exist in the attributes array
     *
     * The default is '*' which means any value can be used
     *
     * @var array
     */
    protected $attributes = ['*'];

    /**
     * Items in this series
     *
     * @var array
     */
    private $items = [];

    /**
     * Attributes which can be directly filled or set, each value must also exist in the attributes array and
     * the case used must be the same, e.g. userID is not the same as userid
     *
     * The default is '*' which means all attributes can be updated
     *
     * @var array
     */
    protected $mutable = ['*'];

    /**
     * Repository classes which can be used to extend a repository
     *
     * @var array
     */
    protected $repositories = [];

    /**
     * Add an array of attributes to the attributes array if they don't already exist
     *
     * @param array $attributes The attributes to add
     *
     * @return void
     */
    protected function addAttributes(array $attributes) : void
    {
        foreach ($attributes as $attribute) {
            // Skip wild cards
            if ($attribute === '*') {
                continue;
            }

            // Add attribute only if attribute doesn't already exist to avoid case differences
            if (!$this->hasAttribute($attribute)) {
                $this->attributes[] = $attribute;
            }
        }
    }

    /**
     * Add an array of mutable attributes to the mutable array if they don't already exist
     *
     * @param array $attributes The attributes to add
     *
     * @return void
     */
    protected function addMutable(array $attributes) : void
    {
        foreach ($attributes as $attribute) {
            // Skip wild cards
            if ($attribute === '*') {
                continue;
            }

            // Add attribute only if attribute doesn't already exist to avoid case differences
            if (!$this->hasMutable($attribute)) {
                $this->mutable[] = $attribute;
            }
        }
    }

    /**
     * Ensure the attributes array contains mutable items and repositories
     *
     * @return void
     */
    private function assertAttributes() : void
    {
        $this->addAttributes(array_merge($this->mutable, array_keys($this->repositories)));
    }

    /**
     * Set mutable items from an array
     *
     * @param array $array The data to update the object from
     *
     * @return mixed This repository
     */
    public function fill(array $array) : self
    {
        foreach ($array as $attribute => $value) {
            // Only update mutable
            if (!$this->hasMutable($attribute)) {
                continue;
            }

            // Preserve all values by casting nulls to strings
            $method = 'set' . Str::studly($attribute);
            $this->$method($value ?? '');
        }

        return $this;
    }

    /**
     * Get a repository class name from an attribute, this method uses the $repositories array in order
     * to find a repository from it's short name
     *
     * @param string $attribute The attribute to find the repository for
     *
     * @return string The name spaced name to the repository
     */
    private function getRepositoryFromAttribute(string $attribute) : string
    {
        // If attribute is set in repositories array and it's a valid class, used it as the repository
        if (array_key_exists($attribute, $this->repositories) && class_exists($this->repositories[$attribute])) {
            return $this->repositories[$attribute];
        }

        // Use standard repository
        return Repository::class;
    }

    /**
     * Determine if an attribute exists
     *
     * @param string $attribute The attribute to check
     *
     * @return bool
     */
    protected function hasAttribute(string $attribute) : bool
    {
        return (bool)$this->resolveAttribute($attribute);
    }

    /**
     * Determine if an attribute exists that is mutable
     *
     * @param string $attribute The attribute to check
     *
     * @return bool
     */
    private function hasMutable(string $attribute) : bool
    {
        return (bool)$this->resolveMutable($attribute);
    }

    /**
     * Instantiate a repository from a sub-value
     *
     * @param string $attribute The attribute for this repository
     * @param array $data The data array to instantiate the repository with
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository The created repository
     */
    private function instantiateRepository(string $attribute, array $data = null) : Repository
    {
        // Get repository class name to use from attribute
        $className = $this->getRepositoryFromAttribute($attribute);

        // If repository has an init method invoke that, otherwise use repository class
        $repository = new $className;
        return method_exists($repository, 'init') ? $repository->init($data) : new $className($data);
    }

    /**
     * Resolve an key within an array, this will resolve an attribute in a case insensitive way, this method
     * will convert using best guess methodology with exact match, camelCase or a lower case, snake string
     *
     * e.g.
     * UserID = user_id
     * UserId = user_id
     * TestUSERID = test_userid
     *
     * @param string $key The key to resolve
     * @param array $array The array to resolve the key within
     *
     * @return string|null The resolved key name or null if attribute doesn't exist
     */
    private function resolve(string $key, array $array) : ?string
    {
        // Loop through the repository and try to find the key without case sensitivity
        foreach ($array as $attribute) {
            if (mb_strtolower($key) === mb_strtolower((string)$attribute)) {
                return $attribute;
            }
        }

        // Grab string parts to prevent items like ID being converted to i_d
        preg_match_all('/([A-Z]+)($|[A-Z][\da-z]+|[\d])/m', $key, $parts);

        // Convert parts found to camel case, will convert parts such as ID to Id
        $attribute = $key;
        foreach ($parts[1] as $part) {
            $attribute = str_replace($part, ucfirst(mb_strtolower($part)), $attribute);
        }

        // Convert result to snake case
        $snake = Str::snake($attribute);

        if (in_array($snake, $array, true)) {
            return $snake;
        }

        // As a last ditch effort try to find a matching attribute without case or punctuation
        $stripped = mb_strtolower(preg_replace('/[^\da-zA-Z]/', '', $key));

        foreach ($array as $attribute) {
            // Test against attribute with case and punctuation removed
            if ($stripped === mb_strtolower(preg_replace('/[^\da-zA-Z]/', '', (string)$attribute))) {
                return $attribute;
            }
        }

        // Array is wide open
        if (in_array('*', $array, true)) {
            // Attempt to find key in data array without case sensitivity, otherwise use passed key
            return $this->resolve($key, array_keys($this->items)) ?? $key;
        }

        // Attribute is invalid, return null
        return null;
    }

    /**
     * Resolve the value for an attribute within the attributes array
     *
     * @param string $attribute The attribute to resolve within the attributes array
     *
     * @return string|null The resolved attribute or null if attribute doesn't exist
     */
    protected function resolveAttribute(string $attribute) : ?string
    {
        return $this->resolve($attribute, $this->attributes);
    }

    /**
     * Resolve the value for an attribute within the mutable array
     *
     * @param string $attribute The attribute to resolve within the mutable array
     *
     * @return string|null The resolved attribute or null if attribute isn't mutable
     */
    private function resolveMutable(string $attribute) : ?string
    {
        return $this->resolve($attribute, $this->mutable);
    }
}
