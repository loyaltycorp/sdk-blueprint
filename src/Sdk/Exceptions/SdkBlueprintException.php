<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Exceptions;

use Exception;

class SdkBlueprintException extends Exception
{
    //
}
