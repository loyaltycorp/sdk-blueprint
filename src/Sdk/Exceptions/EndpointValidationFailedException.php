<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Exceptions;

class EndpointValidationFailedException extends SdkBlueprintException
{
    //
}
