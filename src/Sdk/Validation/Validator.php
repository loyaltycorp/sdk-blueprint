<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Validation;

class Validator
{
    /**
     * Errors from validation
     *
     * @var array
     */
    private $errors = [];

    /**
     * Repository validation rules
     *
     * @var array
     */
    public $rules = [];

    /**
     * Get validation errors array
     *
     * @return array Validation errors
     */
    public function getErrors() : array
    {
        return $this->errors;
    }

    /**
     * Convert the rules into a more readable format
     *
     * @param array $ruleset The ruleset to parse
     *
     * @return void
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\EndpointValidationFailedException If a rule is used which doesn't exist
     */
    private function parseRules(array $ruleset) : void
    {
        foreach ($ruleset as $attribute => $rules) {
            // If rules aren't an array parse string
            if (!is_array($rules)) {
                preg_match_all('/([a-zA-Z\d]+)(?:\:(\S+))?/', (string)$rules, $matches);

                // Reconstruct rules array
                $rules = [];
                foreach (array_keys($matches[0]) as $key) {
                    $rules[$matches[1][$key]] = $matches[2][$key];
                }
            }

            // Set up placeholder for attribute
            $this->rules[$attribute] = [];

            // Rules which require a parameter array
            $parameterArrayRules = ['enum', 'requiredWith', 'requiredWithout'];

            // Process each rule
            foreach ($rules as $rule => $parameters) {
                // If rule is numeric it's an array without parameters
                if (is_numeric($rule)) {
                    $rule = $parameters;
                    $parameters = null;
                }

                // Sort out parameters if the rule requires an array
                if (is_string($parameters) && in_array($rule, $parameterArrayRules, true)) {
                    $parameters = explode(',', $parameters);
                }

                // Attempt to find rule class from rule name
                $namespaced = 'LoyaltyCorp\\SdkBlueprint\\Sdk\\Validation\\Rules\\' . ucfirst($rule);
                if (!class_exists($namespaced)) {
                    // Rule is unknown, add as error
                    $this->errors[] = sprintf("Unknown rule '%s' used for validation", $rule);
                    continue;
                }

                // Add rule to rules array
                $this->rules[$attribute][$namespaced] = $parameters;
            }
        }
    }

    /**
     * Validate a repository
     *
     * @param array $data The data to validate
     * @param array $ruleset The ruleset to validate against
     *
     * @return bool Whether the validation passes or fails
     */
    public function validate(array $data, array $ruleset) : bool
    {
        // Reset errors
        $this->errors = [];

        // Break/format rules into a readable format
        $this->parseRules($ruleset);

        // Perform validation against remaining rules
        foreach ($this->rules as $attribute => $rules) {
            foreach ($rules as $rule => $parameters) {
                /* @var $rule \LoyaltyCorp\SdkBlueprint\Sdk\Validation\Rule */
                $rule = new $rule($attribute, $parameters, $data);

                if (!$rule->validate()) {
                    // Allow multiple errors per attribute
                    if (!array_key_exists($attribute, $this->errors) || !is_array($this->errors[$attribute])) {
                        $this->errors[$attribute] = [];
                    }

                    $this->errors[$attribute][] = $rule->getError();
                }
            }
        }

        // Return result based on errors
        return !count($this->errors);
    }
}
