<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Validation\Rules;

use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\SeriesInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Validation\Rule;

class NotEmpty extends Rule
{
    /**
     * 'NotEmpty' rule
     *
     * @return void
     */
    protected function process() : void
    {
        // Capture value
        $value = $this->getValue();

        // If value is a collection or repository, convert to array
        if ($value instanceof SeriesInterface) {
            $value = $value->toArray();
        }

        // Ensure not empty
        if (empty($value) && $this->hasValue()) {
            $this->error = $this->attribute . ' must not be empty';
        }
    }
}
