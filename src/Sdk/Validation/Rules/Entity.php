<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Validation\Rules;

use LoyaltyCorp\SdkBlueprint\Sdk\Entity as EntityInstance;
use LoyaltyCorp\SdkBlueprint\Sdk\Validation\Rule;

class Entity extends Rule
{
    /**
     * 'email' rule
     *
     * @return void
     */
    protected function process() : void
    {
        // If attribute isn't an entity instance, validation fails
        if ($this->hasValue() && !$this->getValue() instanceof EntityInstance) {
            $this->error = $this->attribute . ' must be an entity';
        }
    }
}
