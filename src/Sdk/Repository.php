<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk;

use Countable;
use DateTime;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\SeriesInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Attributable;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Primable;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Validatable;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Arrayable;
use Illuminate\Support\Str;
use IteratorAggregate;

class Repository implements Countable, IteratorAggregate, SeriesInterface
{
    use Arrayable, Attributable, Primable, Validatable;

    /**
     * Create a new repository
     *
     * @param array $data The data to pre-load the repository with
     */
    public function __construct(array $data = null)
    {
        // Ensure repositories and mutable attributes exists in attribute array
        $this->assertAttributes();

        // Ensure primary key exists in attribute array
        $this->assertPrimaryKey();

        // If not data has been passed there is nothing to do
        if (!is_array($data)) {
            return;
        }

        // If data is a non-associative array containing a single value which isn't a valid attribute
        // set the value to the primary key
        if (count($data) === 1 && key($data) === 0 && !$this->hasAttribute('0')) {
            $data = $this->convertToPrimary($data);
        }

        // Update attributes ignoring errors to allow recreation of objects
        foreach ($data as $attribute => $value) {
            // Force attribute to be string
            $attribute = (string)$attribute;

            // If attribute is invalid, skip
            if (!$this->hasAttribute($attribute)) {
                continue;
            }

            // Resolve attribute to method setAttribute()
            $attribute = (string)$this->resolveAttribute($attribute);
            $method = 'set' . Str::studly($attribute);

            // Prefer extended functionality over direct set
            method_exists($this, $method) ?
                $this->$method($value) :
                $this->set($attribute, $value);
        }
    }

    /**
     * Allow magic getting, checking and setting of fields, e.g. getFieldName() can be used to return
     * $data['fieldName'] or $data['field_name'], hasFieldName() will check if it exists etc
     *
     * This method searches case insensitive
     *
     * @param string $method The method being called
     * @param array $parameters Parameters passed to the method
     *
     * @return mixed The value of the attribute, a boolean or this instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException If the method isn't valid
     */
    public function __call(string $method, array $parameters)
    {
        // Set available types
        $types = ['get', 'has', 'is', 'set'];

        // Break calling method into type (get, has, is, set) and attribute
        preg_match('/^(' . implode('|', $types) . ')([\da-z_]+)/i', $method, $matches);
        $type = mb_strtolower($matches[1] ?? '');
        $attribute = $matches[2] ?? '';

        // The attribute being accessed must exist, the type must be valid and if setting an attribute
        // the user has permission to update it, if one of these things aren't true throw exception
        if ($type === null ||
            !$this->hasAttribute($attribute) ||
            ($type === 'set' && !$this->canSet($attribute))) {
            throw new UndefinedMethodException('Undefined method: ' . $method);
        }

        // Run method and return
        return call_user_func_array([$this, $type], array_merge([$attribute], $parameters));
    }

    /**
     * Determine if a value can be set or not
     *
     * @param string $attribute The attribute being set
     *
     * @return bool
     */
    private function canSet(string $attribute) : bool
    {
        return
            ($this->isPrimaryKey($this->resolveAttribute($attribute)) && !$this->getPrimaryValue()) ||
            $this->hasMutable($attribute);
    }

    /**
     * Enforce a collection is returned by a getter
     *
     * @param string $attribute The attribute the collection is attached to
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection Will always return a collection
     */
    protected function enforceCollection(string $attribute) : Collection
    {
        if (!$this->get($attribute) instanceof Collection) {
            $this->set($attribute, new Collection);
        }

        return $this->get($attribute);
    }

    /**
     * Enforce a repository is returned by a getter
     *
     * @param string $attribute The attribute the expected repository is attached to
     * @param string $repository The class of the expected repository
     *
     * @return mixed The expected repository
     */
    protected function enforceRepository(string $attribute, string $repository) : Repository
    {
        if (!$this->get($attribute) instanceof $repository) {
            $this->set($attribute, new $repository);
        }

        return $this->get($attribute);
    }

    /**
     * Get the first item within this series
     *
     * @return mixed The first item
     */
    public function first()
    {
        return reset($this->items);
    }

    /**
     * Take any date and format it correctly using \DateTime
     *
     * @param string $date The date to format
     * @param string $format The requested format
     *
     * @return string The formatted date
     */
    protected function formatDate(string $date, string $format) : string
    {
        $dateTime = new DateTime($date);
        return $dateTime->format($format);
    }

    /**
     * Get a value from the repository based on attribute
     *
     * @param string $attribute The attribute to get from the data array
     *
     * @return mixed The value within the repository
     */
    protected function get(string $attribute)
    {
        // Return value if it's set or null if it isn't
        return $this->items[$this->resolveAttribute($attribute)] ?? null;
    }

    /**
     * Get nth item from the items
     *
     * @param int $nth The item to get
     *
     * @return mixed
     */
    public function getNth(int $nth)
    {
        // Loop through items and attempt to find the requested item
        $index = 1;
        foreach ($this->items as $item) {
            // Index starts at 0 in array but 1 for nth
            if ($nth === $index) {
                return $item;
            }

            ++$index;
        }

        // Return null/item not found
        return null;
    }

    /**
     * Determine if an attribute exists and is not null
     *
     * @param string $attribute The attribute to search for
     *
     * @return bool Whether the attribute exists or not
     */
    protected function has(string $attribute) : bool
    {
        // Get value
        $value = $this->get($attribute);

        // If attribute is null, the repository doesn't have this attribute
        if ($value === null) {
            return false;
        }

        // If value is a repository or collection, check count of items
        if ($value instanceof SeriesInterface) {
            return $value->count() > 0;
        }

        // Value exists
        return true;
    }

    /**
     * Determine if a value is true or false
     *
     * @param string $attribute The attribute to search for
     *
     * @return bool Whether the attribute is true or false, will evaluate truthy/falsey
     *
     * @SuppressWarnings(PHPMD.ShortMethodName) This method is short to be __call() compatible
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod) This method is only called via __call()
     */
    private function is(string $attribute) : bool
    {
        return (bool)$this->get($attribute);
    }

    /**
     * Get the last item in this series
     *
     * @return mixed The last item
     */
    public function last()
    {
        return end($this->items);
    }

    /**
     * Set a value to the repository
     *
     * @param string $attribute The attribute to set to the repository
     * @param mixed $value The value to set to the attribute
     *
     * @return mixed The original repository for chainability
     */
    protected function set(string $attribute, $value) : self
    {
        // If the value is an array, convert to a repository
        if (is_array($value)) {
            // If there is an array of arrays, convert values to collection of repositories
            $collection = [];
            if (isset($value[0])) {
                foreach ($value as $subValue) {
                    // If sub value is already a repository, add and move on
                    if ($subValue instanceof self) {
                        $collection[] = $subValue;
                        continue;
                    }

                    // If sub value isn't null or an array convert to array
                    $subValue = is_scalar($subValue) ? [$subValue] : $subValue;
                    $collection[] = $this->instantiateRepository($attribute, $subValue);
                }
            }

            $value = count($collection) || empty($value) ?
                new Collection($collection) :
                $this->instantiateRepository($attribute, $value);
        }

        // Update the resolved attribute
        $this->items[$this->resolveAttribute($attribute)] = $value;

        // Sort keys
        ksort($this->items);

        // Make original repository chainable
        return $this;
    }
}
