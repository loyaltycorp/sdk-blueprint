<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ResourceFilterInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ResourcePaginationInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Resources\Filter;
use LoyaltyCorp\SdkBlueprint\Sdk\Resources\Pagination;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Requestable;

/**
 * This class uses magic methods to access attributes
 *
 * @property array attributes Attributes from \LoyaltyCorp\SdkBlueprint\Sdk\Respository
 * @property array mutable Mutable from \LoyaltyCorp\SdkBlueprint\Sdk\Respository
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren) All resources ultimately extend this class
 */
abstract class Resource extends Repository implements EndpointInterface
{
    use Requestable;

    /** @var ResourceFilterInterface */
    private $filter;

    /** @var ResourcePaginationInterface */
    private $pagination;

    /**
     * Create a new resource.
     *
     * @param ResourceFilterInterface|null     $filter
     * @param ResourcePaginationInterface|null $pagination
     */
    public function __construct(ResourceFilterInterface $filter = null, ResourcePaginationInterface $pagination = null)
    {
        $this->filter = $filter ?? new Filter($this->attributes);
        $this->pagination = $pagination ?? new Pagination(['limit' => 100, 'offset' => 0, 'order' => 'asc']);

        // Force non-mutable
        $this->mutable = [];
        // For empty attributes
        $this->attributes = [];

        parent::__construct([]);
    }

    /**
     * Override __call method to accept 'where' requests
     *
     * @see \LoyaltyCorp\SdkBlueprint\Sdk\Repository::__call()
     *
     * @param string $method The method being called
     * @param array $parameters Parameters passed to the method
     *
     * @return mixed The value of the attribute, a boolean or this instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException If the method isn't valid
     */
    public function __call(string $method, array $parameters)
    {
        // Set available types
        $types = ['get', 'has', 'where'];

        // Break calling method into type (get, has, is, set) and attribute
        preg_match('/^(' . implode('|', $types) . ')([A-Z][\da-zA-Z_]+)/', $method, $matches);
        $type = $matches[1] ?? null;
        $attribute = $matches[2] ?? '';

        // Pass through get/has
        if (in_array($type, ['get', 'has'], true)) {
            return parent::__call($method, $parameters);
        }

        // If this is not a where call, or the attribute being accessed must exist
        if ($type === null || !$this->hasAttribute($attribute)) {
            throw new UndefinedMethodException('Undefined method: ' . $method);
        }

        // Set filter
        return $this->where($attribute, reset($parameters));
    }

    /**
     * Return the query filter
     *
     * @return ResourceFilterInterface
     */
    public function getFilter() : ResourceFilterInterface
    {
        return $this->filter;
    }

    /**
     * Return the pagination object
     *
     * @return ResourcePaginationInterface
     */
    public function getPaginator() : ResourcePaginationInterface
    {
        return $this->pagination;
    }

    /**
     * Get the compiled query string
     *
     * @return string
     */
    public function getQueryString(): string
    {
        return http_build_query(array_merge(
            $this->toArray(),
            $this->getFilter()->toQueryArray(),
            $this->getPaginator()->toQueryArray()
        ));
    }

    /**
     * Add a where clause for lookups
     *
     * @param string $attribute The attribute to add to the query
     * @param mixed $value The value to append to the attribute
     *
     * @return mixed The originating resource
     */
    protected function where(string $attribute, $value) : Resource
    {
        $this->getFilter()->where($attribute, $value);

        // Make chainable
        return $this;
    }
}
