<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Resources;

use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ResourceFilterInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @property array attributes Attributes from \LoyaltyCorp\SdkBlueprint\Sdk\Repository
 */
class Filter extends Repository implements ResourceFilterInterface
{
    /**
     * Create a new filter
     *
     * @param array $data This will be ignored as data can't be set to a collection
     */
    public function __construct(array $data = null)
    {
        // Data will contain attributes that are available to filter on
        $this->attributes = $data;

        // Pass through to parent without data
        parent::__construct();
    }

    /**
     * Apply filter on attribute with given value.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return void
     */
    public function where(string $attribute, $value) : void
    {
        $method = sprintf('set%s', ucfirst($attribute));
        $this->$method($value);
    }

    /**
     * Get array representation.
     *
     * @return array
     */
    public function toQueryArray(): array
    {
        return ['filter' => $this->toArray()];
    }
}
