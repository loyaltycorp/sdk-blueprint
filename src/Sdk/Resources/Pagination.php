<?php

namespace LoyaltyCorp\SdkBlueprint\Sdk\Resources;

use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ResourcePaginationInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

class Pagination extends Repository implements ResourcePaginationInterface
{
    /**
     * All the attributes for this repository
     *
     * @var array
     */
    protected $attributes = [
        'limit',
        'offset',
        'order',
    ];

    /**
     * Get array representation.
     *
     * @return array
     */
    public function toQueryArray(): array
    {
        return $this->toArray();
    }
}
