<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Requestable;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface;

/**
 * This class uses magic methods to access attributes
 *
 * @property array $resource The intial resource for this entity
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren) All entities ultimately extend this class
 */
abstract class Entity extends Repository implements EndpointInterface
{
    use Requestable;

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [];

    /**
     * Overload getId to get the primary key if id doesn't exist
     *
     * @return mixed
     */
    public function getId()
    {
        // Return id or primary value if id doesn't exist
        return $this->has('id') ? $this->get('id') : $this->getPrimaryValue();
    }

    /**
     * Overload hasId to check the primary key if id doesn't exist
     *
     * @return mixed
     */
    public function hasId()
    {
        // Return result of id or primary value if id doesn't exist
        return (bool)$this->getId();
    }

    /**
     * Build query string from entity data
     *
     * @return string
     */
    public function getQueryString(): string
    {
        return http_build_query($this->toArray());
    }
}
