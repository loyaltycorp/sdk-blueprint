<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Interfaces;

interface EndpointInterface
{
    /**
     * Get mappings for responses
     *
     * @param string $method The method being used
     *
     * @return array The array from $this->mappings
     */
    public function getMappings(string $method = null) : array;

    /**
     * Get validation errors array
     *
     * @return array Validation errors
     */
    public function getValidationErrors() : array;

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\MethodNotSupportedException If the method requested is not valid
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\AttributeNotSetException If the endpoint requires an attribute which is empty
     */
    public function getEndpointFromMethod(string $method) : string;

    /**
     * Get the endpoint version for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\MethodNotSupportedException If the method requested is not valid
     */
    public function getEndpointVersionFromMethod(string $method) : string;

    /**
     * Remap response fields from one key to another
     *
     * @param array $data The data array to remap
     * @param string $method The method being used
     *
     * @return array
     */
    public function remapResponse(array $data, string $method = null) : array;

    /**
     * Return the request data as array
     *
     * @return array The request data as an array
     */
    public function toArray() : array;

    /**
     * Validate request against a ruleset
     *
     * @param string $method The method to validate agains
     *
     * @return bool The validation status
     */
    public function validate(string $method = null) : bool;

    /**
     * Get HTTP query string for request.
     *
     * @return string
     */
    public function getQueryString() : string;
}
