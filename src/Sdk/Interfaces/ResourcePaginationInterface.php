<?php

namespace LoyaltyCorp\SdkBlueprint\Sdk\Interfaces;

interface ResourcePaginationInterface
{
    /**
     * Get array representation.
     *
     * @return array
     */
    public function toQueryArray() : array;
}
