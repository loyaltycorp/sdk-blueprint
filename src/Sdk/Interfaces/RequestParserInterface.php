<?php

namespace LoyaltyCorp\SdkBlueprint\Sdk\Interfaces;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidBaseUrlException;

interface RequestParserInterface
{
    /**
     * Get request parameters for the HTTP client.
     *
     * @return array
     */
    public function getParameters() : array;

    /**
     * Get URL for the HTTP client.
     *
     * @return string
     */
    public function getUrl() : string;

    /**
     * Set base url.
     *
     * @param string $baseUrl
     *
     * @return RequestParserInterface
     *
     * @throws InvalidBaseUrlException If base url is invalid
     */
    public function setBaseUrl(string $baseUrl) : self;

    /**
     * Set method.
     *
     * @param string $method
     *
     * @return RequestParserInterface
     */
    public function setMethod(string $method) : self;

    /**
     * Set endpoint.
     *
     * @param EndpointInterface $endpoint
     *
     * @return RequestParserInterface
     */
    public function setEndpoint(EndpointInterface $endpoint) : self;
}
