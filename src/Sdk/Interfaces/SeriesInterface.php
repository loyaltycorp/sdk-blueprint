<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk\Interfaces;

use ArrayIterator;

interface SeriesInterface
{
    /**
     * Convert series to string
     *
     * @return string
     */
    public function __toString() : string;

    /**
     * Get the number of items in this series
     *
     * @return int The number of items in this series
     */
    public function count() : int;

    /**
     * Get the first item within this series
     *
     * @return mixed The first item
     */
    public function first();

    /**
     * Get an iterator for the items
     *
     * @return \ArrayIterator
     */
    public function getIterator() : ArrayIterator;

    /**
     * Get the last item in this series
     *
     * @return mixed The last item
     */
    public function last();

    /**
     * Recursively convert a series to array
     *
     * @return array All data from the series in array format
     */
    public function toArray() : array;
}
