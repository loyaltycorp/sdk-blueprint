<?php

namespace LoyaltyCorp\SdkBlueprint\Sdk\Interfaces;

interface ResourceFilterInterface
{
    /**
     * Apply filter on attribute with given value.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return void
     */
    public function where(string $attribute, $value) : void;

    /**
     * Get array representation.
     *
     * @return array
     */
    public function toQueryArray() : array;
}
