<?php

namespace LoyaltyCorp\SdkBlueprint\Sdk\Interfaces;

use Psr\Http\Message\ResponseInterface;

interface ResponseParserInterface
{
    /**
     * Parse response and return data array.
     *
     * @param ResponseInterface $response
     *
     * @return array
     */
    public function parseResponse(ResponseInterface $response) : array ;

    /**
     * Parse error response and return data array.
     *
     * @param ResponseInterface $response
     *
     * @return array
     */
    public function parseErrorResponse(ResponseInterface $response) : array ;
}
