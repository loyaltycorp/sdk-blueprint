<?php

namespace LoyaltyCorp\SdkBlueprint\Sdk\Interfaces;

use LoyaltyCorp\SdkBlueprint\Sdk;
use LoyaltyCorp\SdkBlueprint\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Response;

interface ClientInterface
{
    /**
     * Perform a post request
     *
     * @param Entity $entity The entity to create
     *
     * @return Response Api response object
     */
    public function create(Entity $entity) : Response;

    /**
     * Perform a delete request
     *
     * @param Entity $entity The entity to delete
     *
     * @return Response Api response object
     */
    public function delete(Entity $entity) : Response;

    /**
     * Perform a get request
     *
     * @param Entity $entity The entity to get
     *
     * @return Response Api response object
     */
    public function get(Entity $entity) : Response;

    /**
     * List entities within a resource
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Resource $resource The resource to find
     *
     * @return Response Api response object
     */
    public function list(Sdk\Resource $resource) : Response;

    /**
     * Perform a put request
     *
     * @param Entity $entity The entity to update
     *
     * @return Response Api response object
     */
    public function update(Entity $entity) : Response;
}
