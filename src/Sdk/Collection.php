<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk;

use Countable;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidCollectionException;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\SeriesInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Arrayable;
use IteratorAggregate;

/**
 * @property array items Items from \LoyaltyCorp\SdkBlueprint\Sdk\Traits\Arrayable
 */
class Collection implements Countable, IteratorAggregate, SeriesInterface
{
    use Arrayable;

    /**
     * Create a new collection
     *
     * @param array $items The items to set to the collection
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidCollectionException If the collection is invalid
     */
    public function __construct(array $items = null)
    {
        // If no items are passed, skip
        if ($items === null) {
            return;
        }

        // Loop through items
        foreach ($items as $key => $item) {
            // If key is not numeric, this isn't a collection
            if (!is_numeric($key)) {
                throw new InvalidCollectionException('Collections can only be used for non-associative arrays');
            }

            // If item is an array, convert to repository
            $this->add($item);
        }
    }

    /**
     * Add an item to the collection
     *
     * @param mixed $item The item to add to the collection
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection This collection
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidCollectionException If the item isn't valid for a collection
     */
    public function add($item) : self
    {
        // If item isn't an array make it one
        if (is_scalar($item)) {
            $item = [$item];
        }

        if (!$item instanceof Repository && !is_array($item)) {
            throw new InvalidCollectionException('Collection items must be a repository or array');
        }

        $this->items[] = $item instanceof Repository ? $item : new Repository($item);

        // Make chainable
        return $this;
    }

    /**
     * Clear all items from a collection
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection This collection
     */
    public function clear() : self
    {
        $this->items = [];

        // Make chainable
        return $this;
    }

    /**
     * Delete an item from the collection
     *
     * @param int $nth The item to delete
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection This collection
     */
    public function deleteNth(int $nth) : self
    {
        // Subtract 1 from nth to get the item to remove, set to -1 if $nth
        // is invalid to prevent random removal
        $nth = $nth > 0 ? $nth - 1 : -1;

        // Only remove if $nth is zero or more and key exists
        if ($nth >= 0 && array_key_exists($nth, $this->items)) {
            unset($this->items[$nth]);
        }

        // Reset keys
        $this->items = array_values($this->items);

        // Make chainable
        return $this;
    }

    /**
     * Get the first item in this series
     *
     * @return mixed The first item
     */
    public function & first()
    {
        $keys = array_keys($this->items);
        return $this->items[reset($keys)];
    }

    /**
     * Get the items from the collection
     *
     * @return array
     */
    public function getItems() : array
    {
        return $this->items;
    }

    /**
     * Get nth item from the items
     *
     * @param int $nth The item to get
     *
     * @return mixed
     */
    public function & getNth(int $nth)
    {
        // Determine nth item, 1 = first item, 2 = second item and so on
        $nth = $nth > 0 ? $nth - 1 : -1;

        // Return item or null if index doesn't exist
        $item = $this->items[$nth] ?? null;
        return $item;
    }

    /**
     * Get the last item in this series
     *
     * @return mixed The last item
     */
    public function & last()
    {
        $keys = array_keys($this->items);
        return $this->items[end($keys)];
    }
}
