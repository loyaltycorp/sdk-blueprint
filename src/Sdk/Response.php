<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint\Sdk;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getCode() Alias of $this->get('code')
 * @method null|string getMessage() Alias of $this->get('message')
 * @method null|RequestInterface getRequest() Alias of $this->get('request')
 * @method null|ResponseInterface getResponse() Alias of $this->get('response')
 * @method null|int getStatusCode() Alias of $this->get('status_code')
 * @method bool hasCode() Alias of $this->has('code')
 * @method bool hasMessage() Alias of $this->has('message')
 * @method bool hasRequest() Alias of $this->has('request')
 * @method bool hasResponse() Alias of $this->has('request')
 * @method bool hasStatusCode() Alias of $this->has('status_code')
 * @method bool isSuccessful() Alias of $this->get('successful')
 */
class Response extends Repository
{
    /**
     * The attributes of this model
     *
     * @var array
     */
    protected $attributes = [
        'code',
        'message',
        'request',
        'response',
        'status_code',
        'successful',
    ];

    /**
     * The mutable attributes of this model
     *
     * Nothing is mutable directly on the response object, resources mutability is set in the resource
     *
     * @var array
     */
    protected $mutable = [];
}
