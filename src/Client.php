<?php declare(strict_types = 1);

namespace LoyaltyCorp\SdkBlueprint;

use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ClientInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\RequestParserInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ResponseParserInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\EndpointValidationFailedException;
use LoyaltyCorp\SdkBlueprint\Sdk\Parsers\JsonRequestParser;
use LoyaltyCorp\SdkBlueprint\Sdk\Parsers\JsonResponseParser;
use LoyaltyCorp\SdkBlueprint\Sdk\Response;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface as PsrRequest;
use Psr\Http\Message\ResponseInterface as PsrResponse;

class Client implements ClientInterface
{
    /**
     * Guzzle HTTP client for requests
     *
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * Api requests parser.
     *
     * @var RequestParserInterface
     */
    private $requestParser;

    /**
     * Api responses parser.
     *
     * @var ResponseParserInterface
     */
    private $responseParser;

    /** @var string */
    private $response;

    /**
     * Create a new SDK client
     *
     * @param Response                     $response
     * @param RequestParserInterface|null  $requestParser
     * @param ResponseParserInterface|null $responseParser
     * @param GuzzleClient|null            $client
     */
    public function __construct(
        Response $response,
        RequestParserInterface $requestParser = null,
        ResponseParserInterface $responseParser = null,
        GuzzleClient $client = null
    )
    {
        $this->response = get_class($response);
        $this->requestParser = $requestParser ?? new JsonRequestParser();
        $this->responseParser = $responseParser ?? new JsonResponseParser();
        $this->client = $client ?? new GuzzleClient();
    }

    /**
     * Perform a post request
     *
     * @param Entity $entity The entity to create
     *
     * @return Response Api response object
     */
    public function create(Entity $entity) : Response
    {
        return $this->send('POST', $entity);
    }

    /**
     * Perform a delete request
     *
     * @param Entity $entity The entity to delete
     *
     * @return Response Api response object
     */
    public function delete(Entity $entity) : Response
    {
        return $this->send('DELETE', $entity);
    }

    /**
     * Perform a get request
     *
     * @param Entity $entity The entity to get
     *
     * @return Response Api response object
     */
    public function get(Entity $entity) : Response
    {
        return $this->send('GET', $entity);
    }

    /**
     * Get request parser.
     *
     * @return RequestParserInterface
     */
    protected function getRequestParser() : RequestParserInterface
    {
        return $this->requestParser;
    }

    /**
     * Get response parser.
     *
     * @return ResponseParserInterface
     */
    protected function getResponseParser() : ResponseParserInterface
    {
        return $this->responseParser;
    }

    /**
     * List entities within a resource
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Resource $resource The resource to find
     *
     * @return Response Api response object
     */
    public function list(Sdk\Resource $resource) : Response
    {
        return $this->send('GET', $resource);
    }

    /**
     * Send a request to the API
     *
     * @param string $method The method to use for the request
     * @param EndpointInterface $endpoint The resource to use for the request
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Response Api response object, will catch and filter exceptions
     *
     * @throws EndpointValidationFailedException If validation fails
     */
    private function send(string $method, EndpointInterface $endpoint) : Response
    {
        // Attempt to validate repository, if it's invalid throw a EndpointValidationFailedException
        if (!$endpoint->validate($method)) {
            throw new EndpointValidationFailedException(json_encode($endpoint->getValidationErrors()));
        }

        $requestParser = $this->requestParser->setMethod($method)->setEndpoint($endpoint);

        $url = $requestParser->getUrl();
        $parameters = $requestParser->getParameters();

        return $this->sendRequest($method, $url, $parameters, $endpoint->getMappings($method));
    }

    /**
     * Send request and return response
     *
     * @param string $method The method to use for the request
     * @param string $url The url to send the request to
     * @param array $parameters The parameters to send with the request
     * @param array $mappings Response mappings from the endpoint
     *
     * @return Response Api response object, will catch and filter exceptions
     */
    private function sendRequest(string $method, string $url, array $parameters, array $mappings) : Response
    {
        $responseParser = $this->responseParser;

        // Send request
        try {
            $response = $this->client->request($method, $url, $parameters);
            $data = $responseParser->parseResponse($response);

            // Remap keys if resource requires it
            foreach ($mappings as $from => $to) {
                if (array_key_exists($from, $data)) {
                    $data[$to] = $data[$from];
                    unset($data[$from]);
                }
            }

            // Set status code and successful flag
            $data['status_code'] = $response->getStatusCode();
            $data['successful'] = true;
        } catch (RequestException $exception) {
            // Try to set code and message from api
            $exceptionResponse = $exception->getResponse();
            if ($exceptionResponse instanceof PsrResponse) {
                $data = $responseParser->parseErrorResponse($exception->getResponse());
                $statusCode = $exceptionResponse->getStatusCode();
            }

            // Create data from exception
            $data = [
                'code' => $data['code'] ?? $exception->getCode(),
                'message' => $data['message'] ?? $exception->getMessage(),
                'status_code' => $statusCode ?? 0,
                'successful' => false,
            ];

            // If request and response exists add them to data
            if ($exception->getRequest() instanceof PsrRequest) {
                $data['request'] = $exception->getRequest();
            }

            if ($exception->getResponse() instanceof PsrResponse) {
                $data['response'] = $exception->getResponse();
            }
        }

        // Return response as a repository
        return new $this->response($data);
    }

    /**
     * Perform a put request
     *
     * @param Entity $entity The entity to update
     *
     * @return Response Api response object
     */
    public function update(Entity $entity) : Response
    {
        return $this->send('PUT', $entity);
    }
}
