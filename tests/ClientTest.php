<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\EndpointValidationFailedException;
use GuzzleHttp\Psr7\Request;
use LoyaltyCorp\SdkBlueprint\Sdk\Parsers\JsonRequestParser;
use LoyaltyCorp\SdkBlueprint\Sdk\Parsers\JsonResponseParser;
use LoyaltyCorp\SdkBlueprint\Sdk\Response;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\ClientStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\EntityStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\ResourceStub;

class ClientTest extends TestCase
{
    /**
     * Process a request and get the query string
     *
     * @param mixed $request The request to processed
     *
     * @return string The query string or 'Invalid request' if invalid
     */
    private function getRequestQueryString($request) : string
    {
        return $request instanceof Request ? $request->getUri()->getQuery() : 'Invalid request';
    }

    /**
     * Process a request and get the size
     *
     * @param mixed $request The request to processed
     *
     * @return int The size of the request body or -1 if invalid
     */
    private function getRequestSize($request) : int
    {
        return $request instanceof Request ? $request->getBody()->getSize() : -1;
    }

    /**
     * Test validation failure
     *
     * @return void
     */
    public function testFailedValidation() : void
    {
        // Set up an empty entity with a required rule so validation fails
        $entity = new EntityStub;
        $entity->rules = [
            'value' => 'required',
        ];

        // Attempt to perform a get request, validation should fail
        $this->expectException(EndpointValidationFailedException::class);
        $this->createMockClientStub()->get($entity);
    }

    /**
     * Test getting the request parser
     *
     * @return void
     */
    public function testGetRequestParser() : void
    {
        $client = new ClientStub(new Response);

        $this->assertInstanceOf(JsonRequestParser::class, $client->getRequestParser());
    }

    /**
     * Test getting the response parser
     *
     * @return void
     */
    public function testGetResponseParser() : void
    {
        $client = new ClientStub(new Response);

        $this->assertInstanceOf(JsonResponseParser::class, $client->getResponseParser());
    }

    /**
     * Test request parameters are set correctly for entities
     *
     * @return void
     */
    public function testEntityRequestParameters() : void
    {
        // Set up request data
        $data = ['id' => 2, 'key' => 'value'];

        // Create client with invalid endpoint
        $client = $this->createClientStubWithRequestParser('test://invalid');

        // Create a mock entity
        $ids = ['resource_id' => 'mer_0000000000000000', 'parent_id' => 'par_0000000000000000'];
        $entity = new EntityStub(array_merge($data, $ids));

        // Test each method, since the endpoint isn't valid we should have a response object which is testable
        foreach (['create', 'delete', 'get', 'update'] as $method) {
            /* @var $response \LoyaltyCorp\SdkBlueprint\Sdk\Response */
            $response = $client->$method($entity);

            // Verify response has request which would indicate an error
            $this->assertTrue($response->hasRequest());

            // Run tests based on method
            $request = $response->getRequest();

            switch ($method) {
                // Create (POST) and update (PUT) have request bodies
                case 'create':
                case 'update':
                    $this->assertEquals('', $this->getRequestQueryString($request));
                    $this->assertEquals(mb_strlen((string)$entity), $this->getRequestSize($request));
                    break;

                // Delete and get use query strings
                case 'delete':
                case 'get':
                    $this->assertEquals(http_build_query($data), $this->getRequestQueryString($request));
                    $this->assertEquals(0, $this->getRequestSize($request));
                    break;
            }
        }
    }

    /**
     * Test request parameters are set correctly for resources
     *
     * @return void
     */
    public function testResourceRequestParameters() : void
    {
        // Create client with invalid endpoint
        $client = $this->createClientStubWithRequestParser('test://invalid');

        // Create a mock resource
        $resource = new ResourceStub;

        $response = $client->list($resource);

        // Verify response has request which would indicate an error
        $this->assertTrue($response->hasRequest());

        // Run tests based on method
        $request = $response->getRequest();

        // Test query string
        $this->assertEquals($resource->getQueryString(), $this->getRequestQueryString($request));
        $this->assertEquals(0, $this->getRequestSize($request));
    }

    /**
     * Test response
     *
     * @return void
     */
    public function testResponse() : void
    {
        // Set up response data
        $data = ['message' => 'Hello'];

        // Create client with mock responses
        $client = $this->createMockClientStub([
            $this->createResponse($data),
            $this->createResponse($data, 400),
            $this->createResponse(['mapped' => 'true']),
        ]);

        // Create a mock entity
        $entity = new EntityStub($data);

        // Test successful response
        $response = $client->get($entity);
        $this->assertTrue($response->isSuccessful());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(array_merge($data, ['status_code' => 200, 'successful' => true]), $response->toArray());

        // Test invalid response
        $response = $client->get($entity);
        $this->assertFalse($response->isSuccessful());
        $this->assertEquals(400, $response->getStatusCode());

        // Test response mapping, should've changed 'mapped' to 'code'
        $response = $client->get($entity);
        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->hasCode());
        $this->assertEquals('true', $response->getCode());
    }

    /**
     * Test invalid json response
     *
     * @return void
     */
    public function testInvalidJsonResponse() : void
    {
        // Set up response data
        $data = ['message' => 'Hello'];
        // Create a mock entity
        $entity = new EntityStub($data);

        $this->runRawTests('get', $entity, 'invalid_json');
    }
}
