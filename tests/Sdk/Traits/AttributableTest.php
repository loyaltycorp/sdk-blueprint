<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Exception;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\AttributableStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\GuardedStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\InvalidRepositoryMapStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\RepositoryMapStub;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class AttributableTest extends TestCase
{
    /**
     * Test adding attributes and mutators from constructor
     *
     * @return void
     */
    public function testAttributesFromConstructor() : void
    {
        $repository = new AttributableStub;

        // Ensure attributes/mutables are set, these will throw exception if attributes
        // haven't been set but return false if they have been set because value is null
        $this->assertFalse($repository->hasEmail());
        $this->assertFalse($repository->hasName());

        // Attempt to set name
        $repository->setName('Test');
        $this->assertEquals('Test', $repository->getName());

        // Ensure email can't be set, this will pass if '*' mutable is allowed
        $this->expectException(UndefinedMethodException::class);
        $repository->setEmail('email@test.com');
    }

    /**
     * Ensure repositories don't care about case or punctuation
     *
     * @return void
     */
    public function testBestGuessAttributes() : void
    {
        // Set up array with attributes using different cases
        $data = [
            'user_id' => 1,
            'userName' => 'Test',
            'EMAIladdress' => 'test@test.com',
            'with!Punctuation' => '!',
        ];
        $repository = new Repository($data);

        // Ensure all fields can be found regardless of punctuation of attribute or method
        $this->assertEquals($data['user_id'], $repository->getUserId());
        $this->assertEquals($data['user_id'], $repository->getUserID());
        $this->assertEquals($data['user_id'], $repository->getUSERID());
        $this->assertEquals($data['userName'], $repository->getUserName());
        $this->assertEquals($data['userName'], $repository->getUsername());
        $this->assertEquals($data['userName'], $repository->getUSERNAME());
        $this->assertEquals($data['EMAIladdress'], $repository->getEmailAddress());
        $this->assertEquals($data['EMAIladdress'], $repository->getemailAddress());
        $this->assertEquals($data['EMAIladdress'], $repository->getEMAILADDRESS());
        $this->assertEquals($data['with!Punctuation'], $repository->getWithPunctuation());
        $this->assertEquals($data['with!Punctuation'], $repository->getWithpunctuation());
        $this->assertEquals($data['with!Punctuation'], $repository->GETWITHPUNCTUATION());

        // Make sure it converts back as expected, with punctuation in tact
        $this->assertEquals($data, $repository->toArray());
    }

    /**
     * Test filling a repository mutable attributes
     *
     * @return void
     */
    public function testFillable() : void
    {
        // Set update repository which has mutable attributes set
        $data = [
            'id' => 1,
            'name' => 'Test',
        ];
        $repository = new GuardedStub($data);
        $this->assertEquals($data['id'], $repository->getId());
        $this->assertEquals($data['name'], $repository->getName());

        // Test filling the repository with new data, only name should update
        $updated = [
            'id' => 2,
            'name' => 'Updated',
        ];
        $repository->fill($updated);
        $this->assertEquals($data['id'], $repository->getId());
        $this->assertEquals($updated['name'], $repository->getName());
    }

    /**
     * Test mutable settings are enforced within a repository
     *
     * @return void
     */
    public function testGuardedRepository() : void
    {
        // Set update repository which has mutable attributes set
        $data = [
            'id' => 1,
            'name' => 'Test',
        ];
        $repository = new GuardedStub($data);

        // Test all data was populated by constructor
        $this->assertEquals($data, $repository->toArray());

        // Since name is mutable it should be able to be set
        $repository->setName('New');
        $this->assertEquals('New', $repository->getName());

        // Since id is a valid attribute but not mutable it shouldn't be able to be set
        $this->expectException(Exception::class);
        $repository->setId(1);
    }

    /**
     * Test setting an invalid class to the repositories array
     *
     * @return void
     */
    public function testInvalidRepositoryFromClass() : void
    {
        // Create data using the invalid class via InvalidRepositoryMap stub
        $data = [
            'email' => 'test@test.com',
            'id' => 1,
            'invalid_class' => [
                'id' => 2,
                'name' => 'Invalid',
            ],
            'name' => 'Test',
            'null' => null,
        ];
        $repository = new InvalidRepositoryMapStub($data);

        // Test that invalid class has been set as a repository since InvalidClass isn't a valid class name
        $this->assertTrue($repository->hasInvalidClass());
        $this->assertInstanceOf(Repository::class, $repository->getInvalidClass());
    }

    /**
     * Test setting a repository from repositories array
     *
     * @return void
     */
    public function testRepositoryFromClass() : void
    {
        // Create data using a valid class via RepositoryMap stub
        $data = [
            'email' => 'test@test.com',
            'from_class' => [
                'id' => 2,
                'name' => 'Class',
            ],
            'id' => 1,
            'name' => 'Test',
            'null' => null,
        ];
        $repository = new RepositoryMapStub($data);

        // Test subkey has been converted into FromClass
        $this->assertInstanceOf(RepositoryMapStub::class, $repository->getFromClass());

        // Test root level and FromClass both have correct name
        $this->assertEquals($data['name'], $repository->getName());
        $this->assertEquals($data['from_class']['name'], $repository->getFromClass()->getName());
    }
}
