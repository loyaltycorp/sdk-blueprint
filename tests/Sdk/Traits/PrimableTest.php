<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\GuardedStub;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class PrimableTest extends TestCase
{
    /**
     * Test creating a primary only collection
     *
     * @return void
     */
    public function testPrimaryOnlyCollection() : void
    {
        // Create collection from data
        $data = [[1], [2]];
        $collection = new Collection($data);

        // Test collection contains repositories
        foreach ($collection as $key => $repository) {
            $this->assertInstanceOf(Repository::class, $repository);

            // Test repository is primary only and value has been assigned to primary key
            $this->assertTrue($repository->primaryOnly());
            $this->assertEquals(reset($data[$key]), $repository->getId());
        }
    }

    /**
     * Test primary key is settable once
     *
     * @return void
     */
    public function testSetPrimaryKey() : void
    {
        // Set and test initial repository id
        $repository = new Repository(['id' => 1]);
        $this->assertEquals(1, $repository->getPrimaryValue());

        // Ensure it can be set a second time on an unguarded repository
        $repository->setId(2);
        $this->assertEquals(2, $repository->getPrimaryValue());

        // Set and test on a guarded repository
        $repository = new GuardedStub(['id' => 1]);
        $this->assertEquals(1, $repository->getPrimaryValue());

        // Ensure it can't be set a second time
        $this->expectException(UndefinedMethodException::class);
        $repository->setId(2);
    }
}
