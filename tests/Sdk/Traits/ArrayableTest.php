<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class ArrayableTest extends TestCase
{
    /**
     * Test methods made available by the arrayable trait
     *
     * @return void
     */
    public function testArrayableMethods() : void
    {
        // Create repository
        $data = ['id' => 1];
        $repository = new Repository($data);

        // Test arrayable methods
        $this->assertCount($repository->count(), $data);
        $this->assertEquals(reset($data), $repository->first());
        $this->assertEquals(end($data), $repository->last());
        $this->assertEquals($data, $repository->toArray());
        $this->assertEquals(json_encode($data), (string)$repository);
    }
    
    /**
     * Test conversion of arrays to repositories
     *
     * @return void
     */
    public function testCollectionConversion() : void
    {
        // Create repository from data
        $data = ['ids' => [1, 2]];
        $repository = new Repository($data);

        // Make sure collection was created from the ids
        $this->assertTrue($repository->hasIds());
        $this->assertInstanceOf(Collection::class, $repository->getIds());

        // Get collection
        $collection = $repository->getIds();

        // Test collection contains repositories
        foreach ($collection as $key => $value) {
            $this->assertInstanceOf(Repository::class, $value);

            // Test value has been assigned to primary key
            $this->assertEquals($data['ids'][$key], $value->getId());
        }

        // Test conversion back to array removes 'id' key
        $this->assertEquals($data, $repository->toArray());
    }
}
