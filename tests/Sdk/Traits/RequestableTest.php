<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\AttributeNotSetException;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\MethodNotSupportedException;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\EntityStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\ResourceStub;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class RequestableTest extends TestCase
{
    /**
     * Test parsing variables into endpoints
     *
     * @return void
     */
    public function testEndpointAttributeParsing() : void
    {
        // Set endpoints
        $endpoints = [
            'customers' => [
                'id' => 'res_0000000000000000',
                'parent_id' => 'par_0000000000000000',
                'resource_id' => 'cus_000000000000000000000000',
            ],
            'merchants' => [
                'id' => 'res_0000000000000000',
                'parent_id' => 'par_0000000000000000',
                'resource_id' => 'mer_0000000000000000',
            ],
        ];

        // Loop through endpoints
        foreach ($endpoints as $endpoint => $data) {
            $expected = [
                'delete' => sprintf('%s/%s/test/%s', $endpoint, $data['resource_id'], $data['id']),
                'get' => 'test',
                'post' => sprintf('%s/%s/%s', $endpoint, $data['resource_id'], $data['parent_id']),
                'put' => sprintf('%s/%s/%s/%s', $endpoint, $data['resource_id'], $data['parent_id'], $data['id']),
            ];

            // Use the endpoints stub and populate id/resource id for parsing
            $entity = new EntityStub($data);
            $this->assertEquals($expected['delete'], $entity->getEndpointFromMethod('delete'));
            $this->assertEquals($expected['get'], $entity->getEndpointFromMethod('get'));
            $this->assertEquals($expected['post'], $entity->getEndpointFromMethod('post'));
            $this->assertEquals($expected['put'], $entity->getEndpointFromMethod('put'));
        }

        // Test using an endpoint where the attribute hasn't been set
        $resource = new EntityStub(['id' => 'res_0000000000000000']);
        $this->expectException(AttributeNotSetException::class);
        $resource->getEndpointFromMethod('delete');
    }

    /**
     * Test getting the endpoint version for different methods
     *
     * @return void
     */
    public function testEndpointVersions() : void
    {
        $entity = new EntityStub;
        $this->assertEquals(2, $entity->getEndpointVersionFromMethod('delete'));
        $this->assertEquals(1, $entity->getEndpointVersionFromMethod('get'));
        $this->assertEquals(1, $entity->getEndpointVersionFromMethod('post'));
        $this->assertEquals(1, $entity->getEndpointVersionFromMethod('put'));

        // Test getting a version for a method which is invalid
        $this->expectException(MethodNotSupportedException::class);
        $entity->getEndpointVersionFromMethod('invalid');
    }

    /**
     * Test remapping data
     *
     * @return void
     */
    public function testRemapResponse() : void
    {
        // Set up resource
        $resource = new ResourceStub;

        $data = ['test' => 'value'];

        $this->assertEquals(['value' => 'value'], $resource->remapResponse($data, 'get'));
    }

    /**
     * Test mappings can be retrieved via a getter
     *
     * @return void
     */
    public function testResponseMappingsGetter() : void
    {
        // Set up resource
        $resource = new ResourceStub;

        $this->assertEquals(['test' => 'value'], $resource->getMappings('get'));
        $this->assertEquals(['get' => ['test' => 'value'], 'test' => 'value'], $resource->getMappings());
    }

    /**
     * Test entity/resource unsupported request methods throw exceptions
     *
     * @return void
     */
    public function testUnsupportedRequestMethod() : void
    {
        $resource = new EntityStub;

        // If the method doesn't exist in the endpoints array it's not supported
        $this->expectException(MethodNotSupportedException::class);
        $resource->getEndpointFromMethod('invalid');
    }
}
