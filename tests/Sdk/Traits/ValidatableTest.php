<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Traits;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class ValidatableTest extends TestCase
{
    /**
     * Test validating a collection of repositories with their own rules
     *
     * @return void
     */
    public function testCollectionValidation() : void
    {
        // Create two repositories with different rules
        $repository1 = new Repository(['id' => 1]);
        $repository1->rules = ['id' => 'required'];

        $repository2 = new Repository;
        $repository2->rules = ['name' => 'required'];

        // Create a collection of repositories
        $collection = new Collection([
            $repository1,
            $repository2,
        ]);

        // Create master repository and validate
        $repository = new Repository(['collection' => $collection]);
        $this->assertFalse($repository->validate());

        // Set expected response, only repository 2 (key 1) should fail
        $expected = ['collection' => [1 => ['name' => ['name is required']]]];
        $this->assertEquals($expected, $repository->getValidationErrors());
    }

    /**
     * Test generic validation
     *
     * @return void
     */
    public function testGenericValidation() : void
    {
        $repository = new Repository(['name' => 'Test']);

        // Set repository rules
        $repository->rules = [
            'name' => 'required',
        ];

        // Validation should pass
        $this->assertTrue($repository->validate());
        $this->assertEmpty($repository->getValidationErrors());

        // Remove name and validation should fail
        $repository->setName(null);
        $this->assertFalse($repository->validate());
        $this->assertEquals(['name' => ['name is required']], $repository->getValidationErrors());
    }

    /**
     * Test method specific validation rules
     *
     * @return void
     */
    public function testMethodSpecificValidation() : void
    {
        $repository = new Repository;

        // Test method specific validation
        $repository->rules = [
            'delete,get' => [
                'name' => 'required',
            ],
            'post' => [
                'id' => 'required',
            ],
        ];

        // Validation on get should pass while post should fail
        $repository->setName('Test');
        $this->assertTrue($repository->validate('get'));
        $this->assertFalse($repository->validate('post'));
    }
}
