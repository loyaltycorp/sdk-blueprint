<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidCollectionException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class CollectionTest extends TestCase
{
    /**
     * Test adding an item to a collection
     *
     * @return void
     */
    public function testAddToCollection() : void
    {
        // Create collection
        $collection = new Collection([[1], [2]]);
        $this->assertEquals(2, $collection->count());

        // Add an item
        $collection->add(3);
        $this->assertEquals(3, $collection->count());

        // Add an invalid item
        $this->expectException(InvalidCollectionException::class);
        $collection->add(null);
    }

    /**
     * Test methods made available by the arrayable trait
     *
     * @return void
     */
    public function testArrayableMethods() : void
    {
        // Create collection
        $collection = new Collection([[1], [2]]);

        // Test arrayable methods
        $this->assertEquals(['id' => 1], $collection->first()->toArray());
        $this->assertEquals(['id' => 1], $collection->getNth(1)->toArray());
        $this->assertEquals(['id' => 2], $collection->last()->toArray());
    }

    /**
     * Test getters return references
     *
     * @return void
     */
    public function testCollectionReferences() : void
    {
        // Create collection
        $collection = new Collection([[1], [2]]);

        // Make sure items ARE returned by reference
        $collection->first()->setId(3);
        $this->assertEquals(['id' => 3], $collection->first()->toArray());

        $collection->getNth(1)->setId(4);
        $this->assertEquals(['id' => 4], $collection->getNth(1)->toArray());

        $collection->last()->setId(5);
        $this->assertEquals(['id' => 5], $collection->last()->toArray());
    }

    /**
     * Test creating a collection from data
     *
     * @return void
     */
    public function testCreateCollection() : void
    {
        $collection = new Collection([[1], [2]]);

        // Test values
        $this->assertEquals(2, $collection->count());
        $this->assertInstanceOf(Repository::class, $collection->first());
    }

    /**
     * Test creating an invalid collection
     *
     * @return void
     */
    public function testCreateInvalidCollection() : void
    {
        // Create collection with invalid data, collections only accept non-associative arrays
        $this->expectException(InvalidCollectionException::class);
        new Collection(['id' => 1]);
    }

    /**
     * Test getting original item array
     *
     * @return void
     */
    public function testGetOriginalItems() : void
    {
        $collection = new Collection([[1], [2]]);

        // Set up expectation
        $expected = [
            new Repository([1]),
            new Repository([2]),
        ];

        $this->assertEquals($expected, $collection->getItems());
    }

    /**
     * Test removing items from a collection
     *
     * @return void
     */
    public function testRemovalMethods() : void
    {
        $collection = new Collection([[1], [2]]);

        // Remove the first item
        $collection->deleteNth(1);
        $this->assertEquals([2], $collection->toArray());

        // Remove all items
        $collection->clear();
        $this->assertEquals([], $collection->toArray());
    }
}
