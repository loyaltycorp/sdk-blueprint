<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk;

use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\ResponseStub;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class ResponseTest extends TestCase
{
    /**
     * Test isSuccessful() method on responses
     *
     * @return void
     */
    public function testSuccessResponse() : void
    {
        $response = new ResponseStub;

        // Test bool true
        $response->setSuccessful(true);
        $this->assertTrue($response->isSuccessful());

        // Test bool false
        $response->setSuccessful(false);
        $this->assertFalse($response->isSuccessful());

        // Test non-bool truthy
        $response->setSuccessful('a');
        $this->assertTrue($response->isSuccessful());

        // Test non-bool falsey
        $response->setSuccessful(0);
        $this->assertFalse($response->isSuccessful());
    }
}
