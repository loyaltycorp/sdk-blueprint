<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Validation\Rules;

use LoyaltyCorp\SdkBlueprint\Sdk\Entity;
use Tests\LoyaltyCorp\SdkBlueprint\ValidationTestCase;

class EntityTest extends ValidationTestCase
{
    /**
     * Test 'entity' validation
     *
     * @return void
     */
    public function testEntityValidation() : void
    {
        // Test data
        $this->data = [
            'valid' => $this->getMockForAbstractClass(Entity::class),
            'invalid' => 'invalid',
        ];

        // Run tests
        $this->runValidationTests('entity:' . Entity::class);
    }
}
