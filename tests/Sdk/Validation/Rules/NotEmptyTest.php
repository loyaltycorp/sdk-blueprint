<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Validation\Rules;

use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\SeriesInterface;
use Tests\LoyaltyCorp\SdkBlueprint\ValidationTestCase;

class NotEmptyTest extends ValidationTestCase
{
    /**
     * Test 'notEmpty' validation with array
     *
     * @return void
     */
    public function testNotEmptyValidationWithArray() : void
    {
        // Test data
        $this->data = [
            'valid' => [123],
            'invalid' => [],
        ];

        // Run tests
        $this->runValidationTests('notEmpty');
    }

    /**
     * Test 'notEmpty' validation with SeriesInterface
     *
     * @return void
     */
    public function testNotEmptyValidationWithSeriesInterface() : void
    {
        $series = $this->getMockForAbstractClass(SeriesInterface::class);
        $series->method('toArray')->willReturn(['series_value']);

        // Test data
        $this->data = [
            'valid' => $series,
            'invalid' => [],
        ];

        // Run tests
       $this->runValidationTests('notEmpty');
    }
}
