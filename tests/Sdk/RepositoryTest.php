<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Exception;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\EnforcableStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\GuardedStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\MagicMethodStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\CustomSetterStub;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class RepositoryTest extends TestCase
{
    /**
     * Some test data for the repository
     *
     * @var array
     */
    private $data = [
        'id' => 4,
        'name' => 'Test',
        'null' => null,
    ];

    /**
     * Test methods made available by the arrayable trait
     *
     * @return void
     */
    public function testArrayableMethods() : void
    {
        // Create repository
        $data = ['id' => 1];
        $repository = new Repository($data);

        // Test arrayable methods
        $this->assertEquals(reset($data), $repository->first());
        $this->assertEquals(reset($data), $repository->getNth(1));
        $this->assertEquals(end($data), $repository->last());

        // Make sure invalid nths return null
        $this->assertNull($repository->getNth(2));
        $this->assertNull($repository->getNth(-1));
    }

    /**
     * Test creating a repository
     *
     * @return void
     */
    public function testCreateRepository() : void
    {
        // Create empty repository
        $repository = new Repository;
        $this->assertEquals([], $repository->toArray());

        // Create a repository with some data, only id and name are valid attributes
        $data = [
            'id' => 1,
            'name' => 'Test',
            'invalid' => 'Pineapple',
        ];
        $repository = new GuardedStub($data);

        // Ensure all data was set except 'invalid'
        $this->assertEquals(['id' => $data['id'], 'name' => $data['name']], $repository->toArray());
    }

    /**
     * Test creating a repository with a collection and a collection containing a repository
     *
     * @return void
     */
    public function testCreateRepositoryWithCollection() : void
    {
        // Set data
        $data = [
            'id' => 1,
            'collection' => [1, 2],
            'repository_collection' => [new Repository(['id' => 2])],
        ];

        // Create empty repository
        $repository = new Repository($data);

        // Assert id
        $this->assertEquals($data['id'], $repository->getId());

        // Check collection
        $this->assertTrue($repository->hasCollection());
        $this->assertInstanceOf(Collection::class, $repository->getCollection());

        // Check repository collection
        $repository = $repository->getRepositoryCollection()->first();
        $this->assertInstanceOf(Repository::class, $repository);
        $this->assertEquals(2, $repository->getId());
    }

    /**
     * Test enforcing attributes
     *
     * @return void
     */
    public function testEnforcement() : void
    {
        $entity = new EnforcableStub;

        $this->assertInstanceOf(Collection::class, $entity->getCollection());
        $this->assertInstanceOf(Repository::class, $entity->getRepository());
    }

    /**
     * Test 'get' magic method
     *
     * @return void
     */
    public function testGetMagicMethod() : void
    {
        // Use MagicMethod stub
        $repository = new MagicMethodStub($this->data);

        // Test values which have been set, null should return null
        $this->assertEquals($this->data['id'], $repository->getId());
        $this->assertEquals($this->data['name'], $repository->getName());
        $this->assertNull($repository->getNull());

        // An invalid key should throw an exception
        $this->expectException(Exception::class);
        $this->assertNull($repository->getInvalid());
    }

    /**
     * Test 'has' magic method
     *
     * @return void
     */
    public function testHasMagicMethod() : void
    {
        // Use MagicMethod repository stub
        $repository = new MagicMethodStub($this->data);

        // Test values which have been set, empty should return false
        $this->assertTrue($repository->hasId());
        $this->assertTrue($repository->hasName());
        $this->assertFalse($repository->hasNull());

        // An invalid key should throw an exception
        $this->expectException(Exception::class);
        $this->assertNull($repository->hasInvalid());
    }

    /**
     * Test 'is' magic method
     *
     * @return void
     */
    public function testIsMagicMethod() : void
    {
        // Use MagicMethod repository stub
        $repository = new MagicMethodStub(['false' => 0, 'true' => 'true']);

        // Test values which have been set, should evaluate truthy and falsey
        $this->assertFalse($repository->isFalse());
        $this->assertTrue($repository->isTrue());
    }

    /**
     * Test 'set' magic method
     *
     * @return void
     */
    public function testSetMagicMethod() : void
    {
        // Use MagicMethod repository stub
        $repository = new MagicMethodStub;

        // Set id
        $repository->setId($this->data['id']);
        $this->assertEquals($this->data['id'], $repository->getId());

        // An invalid key should throw an exception
        $this->expectException(Exception::class);
        $this->assertNull($repository->setInvalid(''));
    }

    /**
     * Test a repository with an overridden setter
     *
     * @return void
     */
    public function testSetterOverride() : void
    {
        $repository = new CustomSetterStub($this->data);

        // The setter should have changed the id to id x 100
        $this->assertEquals($this->data['id'] * 100, $repository->getId());
    }
}
