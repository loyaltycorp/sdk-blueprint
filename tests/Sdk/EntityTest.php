<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Entity;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\PrimaryKeyStub;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class EntityTest extends TestCase
{
    /**
     * Test attributes on creation
     *
     * @return void
     */
    public function testConstructor() : void
    {
        /** @var \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity */
        $entity = $this->getMockForAbstractClass(Entity::class);

        // Test base attributes are set, an exception will be thrown if they aren't
        $this->assertNull($entity->getId());
    }

    /**
     * Test get/has id methods
     *
     * @return void
     */
    public function testGetHasId() : void
    {
        // Create entity with no data
        /** @var \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity */
        $entity = $this->getMockForAbstractClass(Entity::class);

        // Test values
        $this->assertNull($entity->getId());
        $this->assertFalse($entity->hasId());

        // Create entity with data in the id
        /** @var \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity */
        $entity = $this->getMockForAbstractClass(Entity::class, [['id' => 1]]);

        // Test values
        $this->assertEquals(1, $entity->getId());
        $this->assertTrue($entity->hasId());

        // Use stub to test a different primary key
        $entity = new PrimaryKeyStub(['primary_id' => 1]);

        // Test values
        $this->assertEquals($entity->getPrimaryValue(), $entity->getId());
        $this->assertTrue($entity->hasId());
        $this->assertEquals(1, $entity->getPrimaryValue());
    }
}
