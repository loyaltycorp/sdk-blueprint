<?php

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\ResourceStub;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class ResourceTest extends TestCase
{
    /**
     * Test exception is thrown when endpoint isn't set
     *
     * @return void
     */
    public function testNoEndpointException() : void
    {
        $parser = new Sdk\Parsers\JsonRequestParser();

        $this->expectException(Sdk\Exceptions\EndpointValidationFailedException::class);
        $parser->getUrl();
    }

    /**
     * Test __call function on Resource object.
     *
     * @return void
     */
    public function testMagicCalls() : void
    {
        $resource = new ResourceStub();

        $this->assertInstanceOf(Sdk\Resource::class, $resource->whereValue('value'));

        $this->assertNull($resource->getValue());

        $this->expectException(UndefinedMethodException::class);
        $resource->whereInvalid('invalid');
    }

    /**
     * Test Timestampable trait functions through Resource object.
     *
     * @return void
     */
    public function testTimestampableMethods() : void
    {
        $resource = new ResourceStub();

        $this->assertInstanceOf(Sdk\Resource::class, $resource->whereCreatedAt('now'));
        $this->assertInstanceOf(Sdk\Resource::class, $resource->whereUpdatedAt('now'));
    }
}
