<?php

namespace Tests\LoyaltyCorp\SdkBlueprint\Sdk\Parsers;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidBaseUrlException;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\RequestParserInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Parsers\JsonRequestParser;
use Tests\LoyaltyCorp\SdkBlueprint\TestCase;

class ApiRequestJsonParserTest extends TestCase
{
    /**
     * Test setting the base url
     *
     * @return void
     */
    public function testSetBaseUrl() : void
    {
        $parser = new JsonRequestParser();

        // Test good url
        $parser = $parser->setBaseUrl('http://localhost');
        $this->assertInstanceOf(RequestParserInterface::class, $parser);

        // Test bad url
        $this->expectException(InvalidBaseUrlException::class);
        $parser->setBaseUrl('localhost');
    }
}
