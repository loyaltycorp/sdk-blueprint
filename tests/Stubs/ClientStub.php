<?php

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs;

use LoyaltyCorp\SdkBlueprint\Client;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\RequestParserInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ResponseParserInterface;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\ResponseStub;

class ClientStub extends Client
{
    protected $baseUrl = 'http://test-base-url.com.au';

    /**
     * Get SDK Response class to return.
     *
     * @return string
     */
    protected function getResponseClass(): string
    {
        return ResponseStub::class;
    }

    /**
     * Make getRequestParser() public
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\RequestParserInterface
     */
    public function getRequestParser() : RequestParserInterface
    {
        return parent::getRequestParser();
    }

    /**
     * Make getResponseParser() public
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\ResponseParserInterface
     */
    public function getResponseParser() : ResponseParserInterface
    {
        return parent::getResponseParser();
    }
}
