<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs;

use LoyaltyCorp\SdkBlueprint\Sdk\Resource as BaseResource;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

class ResourceStub extends BaseResource
{
    use Timestampable;

    public function __construct($filter = null, $pagination = null)
    {
        parent::__construct($filter, $pagination);

        $this->addAttributes([
            'created_at',
            'endDate',
            'startDate',
            'updated_at',
            'value',
        ]);
    }

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => ':resource/:resource_id',
    ];


    /**
     * Mappings for returned values
     *
     * @var array
     */
    protected $mappings = [
        'get' => [
            'test' => 'value',
        ],
        'test' => 'value',
    ];

    /**
     * Define the intial resource for this entity, used where the same data is used
     * for multiple endpoints, e.g. the customer/merchant crossover
     *
     * @var string
     */
    protected $resource = 'test';

    /**
     * Define the resource id for this entity, used where an entity is a child of
     * a resource, usually a customer or merchant
     */
    protected $resourceId = 1;
}
