<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

class AttributableStub extends Repository
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [];

    /**
     * Test setting attribute and mutator from constructor
     *
     * @param array $data The data to prefill the repository with
     */
    public function __construct(array $data = null)
    {
        // Add attributes
        $this->addAttributes(['*', 'email', 'name']);

        // Add mutable
        $this->addMutable(['*', 'name']);

        // Create repository
        parent::__construct($data);
    }
}
