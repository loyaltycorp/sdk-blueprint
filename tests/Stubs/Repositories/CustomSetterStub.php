<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

class CustomSetterStub extends Repository
{
    /**
     * All the attributes for this repository
     *
     * @var array
     */
    protected $attributes = [
        'email',
        'id',
        'name',
        'null'
    ];

    /**
     * Overwrite id setter
     *
     * @param int $id The id to set
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository This stub, chainable
     *
     * @SuppressWarnings(PHPMD.ShortVariableName) This is a test specifically for id
     */
    public function setId(int $id) : Repository
    {
        // Change the id by multiplying it by 100
        return $this->set('id', $id * 100);
    }
}
