<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

class EnforcableStub extends Entity
{
    /**
     * All the attributes for this repository
     *
     * @var array
     */
    protected $attributes = [
        'collection',
        'repository',
    ];

    /**
     * Get collection
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection The collection
     */
    public function getCollection() : Collection
    {
        return $this->enforceCollection('collection');
    }

    /**
     * Get repository
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository The repository
     */
    public function getRepository() : Repository
    {
        return $this->enforceRepository('repository', Repository::class);
    }
}
