<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use LoyaltyCorp\SdkBlueprint\Sdk\Response as BaseResponse;

class ResponseStub extends BaseResponse
{
    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'successful'
    ];

    protected $repositories = [
        'raw' => Repository::class
    ];
}
