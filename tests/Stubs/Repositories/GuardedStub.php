<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

class GuardedStub extends Repository
{
    /**
     * All the attributes for this repository
     *
     * @var array
     */
    protected $attributes = [
        'id',
        'name'
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'name'
    ];
}
