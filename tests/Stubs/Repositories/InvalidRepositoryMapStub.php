<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

class InvalidRepositoryMapStub extends Repository
{
    /**
     * All the attributes for this repository
     *
     * @var array
     */
    protected $attributes = [
        'email',
        'id',
        'name',
        'null'
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'email',
        'id',
        'name',
        'null'
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'invalid_class' => 'InvalidClass',
    ];
}
