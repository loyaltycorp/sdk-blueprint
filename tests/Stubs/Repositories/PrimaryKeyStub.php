<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Entity;

class PrimaryKeyStub extends Entity
{
    /**
     * The primary key for this repository
     *
     * @var string
     */
    protected $primaryKey = 'primary_id';
}
