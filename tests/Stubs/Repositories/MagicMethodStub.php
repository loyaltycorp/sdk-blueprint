<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories;

use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

class MagicMethodStub extends Repository
{
    /**
     * All the attributes for this repository
     *
     * @var array
     */
    protected $attributes = [
        'false',
        'id',
        'name',
        'null',
        'true'
    ];
}
