<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint\Stubs;

use LoyaltyCorp\SdkBlueprint\Sdk\Entity as BaseEntity;

class EntityStub extends BaseEntity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'id',
        'key',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => ':resource/:resource_id/test/:id',
        'get' => 'test',
        'post' => ':resource/:resource_id/:parent_id',
        'put' => ':resource/:resource_id/:parent_id/:id',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpointVersions = [
        'delete' => 2,
        'get' => 1,
        'post' => 1,
        'put' => 1,
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'mapped' => 'code',
    ];

    /**
     * Create a new eWallet instance
     *
     * @param array $data The data to populate the repository with
     */
    public function __construct(array $data = null)
    {
        // Set resource and resource id from data
        $this->setEndpointVariablesFromData($data, ['resource_id'], ['parent_id']);

        // Pass through to entity
        parent::__construct($data);
    }
}
