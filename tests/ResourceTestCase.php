<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint;

use LoyaltyCorp\SdkBlueprint\Sdk;
use Illuminate\Support\Str;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren) All resource test cases extend this class
 */
class ResourceTestCase extends TestCase
{
    /**
     * Test a resource
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Resource $resource The resource to test against
     * @param string $message The message from the api
     * @param array $list The returned entity list
     * @param string $key The key to use in the response array
     *
     * @return void
     */
    protected function runResourceTests(
        Sdk\Resource $resource,
        string $message,
        array $list,
        string $key = 'list'
    ) : void {
        // Set up response data
        $responseData = [
            'code' => 0,
            'message' => $message,
            $key => $list,
        ];

        // Set up client
        $client = $this->createMockClientStub([
            $this->createResponse($responseData),
        ]);

        $this->runResponseTests($resource, $client->list($resource), $responseData);
    }

    /**
     * Test mutators on certain attributes within a filter
     *
     * @param string $resource The resource to test
     * @param array $original The original data to use
     * @param array $expected The expected data after mutation
     *
     * @return void
     */
    public function runFilterAttributeMutatorTests(string $resource, array $original, array $expected) : void
    {
        // Create resource with data
        /** @var \LoyaltyCorp\SdkBlueprint\Sdk\Resource $resource */
        $resource = new $resource($original);

        // Make sure we're working with a resource
        $this->assertInstanceOf(Sdk\Resource::class, $resource);

        // Test attributes have been set on the filter and values are as expected
        foreach ($original as $key => $value) {
            $method = sprintf('where%s', Str::studly($key));
            $resource->$method($value);

            // Get filter value to test
            $method = sprintf('get%s', Str::studly($key));
            $this->assertEquals($expected[$key] ?? '-1', $resource->getFilter()->$method());
        }
    }
}
