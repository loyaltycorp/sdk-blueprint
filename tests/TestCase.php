<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint;

use LoyaltyCorp\SdkBlueprint\Client;
use LoyaltyCorp\SdkBlueprint\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Parsers\JsonRequestParser;
use LoyaltyCorp\SdkBlueprint\Sdk\Response;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Psr\Http\Message\ResponseInterface;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\ClientStub;
use Tests\LoyaltyCorp\SdkBlueprint\Stubs\Repositories\ResponseStub;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren) Everything extends this class
 */
class TestCase extends BaseTestCase
{
    /**
     * Create SDK client stub.
     *
     * @param array $responses
     *
     * @return Client
     */
    protected function createMockClientStub(array $responses = []) : Client
    {
        $guzzleClient = !empty($responses) ? $this->createGuzzleClient($responses) : null;

        return new ClientStub(new ResponseStub(), null, null, $guzzleClient);
    }

    /**
     * Create SDK client stub with request parser.
     *
     * @param string $baseUrl
     *
     * @return Client
     */
    protected function createClientStubWithRequestParser(string $baseUrl) : Client
    {
        $parser = (new JsonRequestParser())->setBaseUrl($baseUrl);

        return new ClientStub(new ResponseStub(), $parser);
    }

    /**
     * Create Guzzle client with mock responses
     *
     * @param array $responses An array of responses to send
     *
     * @return \GuzzleHttp\Client The stacked mock client
     */
    protected function createGuzzleClient(array $responses) : GuzzleClient
    {
        // Set up mock responses
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        return new GuzzleClient(['handler' => $handler]);
    }

    /**
     * Create a mock json response
     *
     * @param array $data The data to add to the response
     * @param int $statusCode The status code to send with the response
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function createResponse(array $data, int $statusCode = 200) : ResponseInterface
    {
        return new GuzzleResponse($statusCode, ['Content-Type' => 'application/json'], json_encode($data));
    }

    /**
     * Test a response
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface $requestable The entity or resource
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Response $response The response recieved
     * @param array $responseData Original response data
     * @param int $status The status code to use
     * @param string $action The action used
     *
     * @return void
     */
    public function runResponseTests(
        EndpointInterface $requestable,
        Response $response,
        array $responseData,
        int $status = 200,
        string $action = null
    ) : void {
        // Test response result
        switch ($status) {
            case 200:
                $this->assertTrue($response->isSuccessful());
                break;

            default:
                $this->assertFalse($response->isSuccessful());
                break;
        }

        // Set up full expected response
        $expected = array_merge($responseData, ['status_code' => $status, 'successful' => $status === 200]);

        // Get method from action
        switch ($action) {
            case 'create':
                $method = 'post';
                break;

            case 'delete':
            case 'get':
                $method = $action;
                break;

            case 'update':
                $method = 'put';
                break;

            default:
                $method = 'get';
                break;
        }

        // Remap fields if required
        $expected = $requestable->remapResponse($expected, $method);

        // Errors should include the request and response
        if (!$response->isSuccessful()) {
            $response = $this->runUnsuccessfulResponseTests($response);
        }

        // Test response matches exactly
        $this->assertEquals($expected, $response->toArray());
    }

    /**
     * Test a raw response
     *
     * @param string $action The action to perform on the client
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity The entity to test against
     * @param string $responseData The response data
     * @param int $status The status code
     *
     * @return void
     */
    protected function runRawTests(
        string $action,
        Entity $entity,
        $responseData,
        $status = 200
    ) : void {

        // Set up client
        $client = $this->createMockClientStub([
            new GuzzleResponse(200, ['Content-Type' => 'image/png'], $responseData),
        ]);

        /** @var \LoyaltyCorp\SdkBlueprint\Sdk\Response $response */
        $response = $client->$action($entity);

        // Test response
        $this->assertTrue($response->isSuccessful());

        // Set up full expected response
        $expected = array_merge(['raw' => $responseData], ['status_code' => $status, 'successful' => $status === 200]);

        // Test response matches exactly
        $this->assertEquals($expected, $response->toArray());
    }

    /**
     * Run tests on an unsuccessful response
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Response $response The response to test
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Response The updated response to continue testing on
     */
    private function runUnsuccessfulResponseTests(Response $response) : Response
    {
        $responseArray = $response->toArray();

        // Test exception is caught correctly
        $this->assertArrayHasKey('request', $responseArray);
        $this->assertInstanceOf(GuzzleRequest::class, $responseArray['request']);
        $this->assertArrayHasKey('response', $responseArray);
        $this->assertInstanceOf(GuzzleResponse::class, $responseArray['response']);

        // Test values without request/response
        unset($responseArray['request'], $responseArray['response']);

        // Recreate response without request/response
        return new Response($responseArray);
    }
}
