<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\SdkBlueprint;

use LoyaltyCorp\SdkBlueprint\Sdk\Validation\Validator;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren) All rules extend this class
 */
class ValidationTestCase extends TestCase
{
    /**
     * Test data
     *
     * @var array
     */
    protected $data;

    /**
     * Run a test against a validation rule
     *
     * @param string $rule The rule to test against
     *
     * @return void
     */
    public function runValidationTests(string $rule) : void
    {
        $validator = new Validator;

        // Test valid value
        $this->assertTrue($validator->validate(['value' => $this->data['valid']], ['value' => $rule]));

        // Test invalid value
        $this->assertFalse($validator->validate(['value' => $this->data['invalid']], ['value' => $rule]));
        $this->assertCount(1, $validator->getErrors());
    }
}
